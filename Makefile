.PHONY: qa fix php-cs-fixer psalm phpstan deptrac

qa: php-cs-fixer psalm phpstan

php-cs-fixer:
	PHP_CS_FIXER_IGNORE_ENV=1 ./vendor/bin/php-cs-fixer --dry-run -v fix

phpstan:
	php -d memory_limit=4G ./vendor/bin/phpstan analyse --no-progress

psalm:
	./vendor/bin/psalm --no-progress --threads=1

deptrac:
	./vendor/bin/deptrac

fix:
	PHP_CS_FIXER_IGNORE_ENV=1 ./vendor/bin/php-cs-fixer -vv fix

