# rva-vzw/krakboem-bundle

Me trying to create a Symfony bundle for rva-vzw/krakboem

(c) Johan Vervloet 2023, 2024

Licensed under [AGPL v3](LICENSE) or later

This bundle tries to provide a Symfony implementation of the interfaces
for event sourcing I created in [rva-vzw/krakboem](https://gitlab.com/rva-vzw/krakboem).

I use symfony messenger for the buses,
and doctrine for the event store. (Which is probably not optimal.
But it will do for now.)
I tried to move normalization to the bundle as well, but I didn't succeed
(yet?)

If you install the bundle, it will set up the service container for you.
But since I didn't find out yet how to configure doctrine and messenger in
a bundle, that's something that needs to be done manually.

There's little documentation for this (experimental) bundle.
But you might want to take a look how the projects below
use it:

* [dikdikdik](https://gitlab.com/rva-vzw/dikdikdik), my famous app to
  keep track of the score of the
  [solo whist](https://en.wikipedia.org/wiki/Solo_whist) card game.
* [wdebelek](https://gitlab.com/rva-vzw/wdebelek), a web app to play
  card games. We used it in Covid times.
* [1jg](https://gitlab.com/johanv/1jg), another score app.


### Installation instructions

```bash
composer require rva-vzw/krakboem-bundle
```

### Configuration

#### messenger

The configuration of the buses:

```yaml
# config/packages/messenger.yaml

framework:
  messenger:
    default_bus: messenger.bus.commands
    buses:
      messenger.bus.events:
        default_middleware: allow_no_handlers
        middleware:
          - 'RvaVzw\KrakBoemBundle\Messenger\EventStoreMiddleware'
      messenger.bus.commands:
      messenger.bus.replay:
        default_middleware: allow_no_handlers
```

You also need to tag your listeners, so that
the correct bus handles the correct messages:

```yaml
# config/services.yaml:

services:
  _instanceof:
    RvaVzw\KrakBoem\EventSourcing\EventListener\EventListener:
      tags: [{ name: messenger.message_handler, bus: messenger.bus.events }]
    RvaVzw\KrakBoem\EventSourcing\Replay\Replayable:
      tags: [{ name: messenger.message_handler, bus: messenger.bus.replay }]
    RvaVzw\KrakBoem\Cqrs\CommandBus\CommandHandler:
      tags: [{ name: messenger.message_handler, bus: messenger.bus.commands }]
```

#### doctrine 

Regarding doctrine, I usually configure two entity managers: one for the event store, and one for the read model.
This is the configuration I use for krakboem:

```yaml
# config/packages/doctrine.yaml:

doctrine:
  dbal:
    url: '%env(resolve:DATABASE_URL)%'

    # IMPORTANT: You MUST configure your server version,
    # either here or in the DATABASE_URL env var (see .env file)
    #server_version: '15'
  orm:
    auto_generate_proxy_classes: true
    default_entity_manager: read_model
    proxy_dir: '%kernel.build_dir%/doctrine/orm/Proxies'
    entity_managers:
      read_model:
        naming_strategy: doctrine.orm.naming_strategy.underscore
        auto_mapping: true
        connection: default
        mappings:
          ReadModel:
            is_bundle: false
            dir: '%kernel.project_dir%/src/Infrastructure/ReadModel'
            prefix: 'App\Infrastructure\ReadModel'
            alias: ReadModel
      event_store:
        naming_strategy: doctrine.orm.naming_strategy.underscore
        connection: default
        mappings:
          EventStore:
            is_bundle: false
            type: attribute
            dir: '%kernel.project_dir%/vendor/rva-vzw/krakboem-bundle/src/Doctrine'
            prefix: 'RvaVzw\KrakBoemBundle\Doctrine'
            alias: EventStore
```

