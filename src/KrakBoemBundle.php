<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoemBundle;

use RvaVzw\KrakBoemBundle\DependencyInjection\KrakBoemExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

final class KrakBoemBundle extends AbstractBundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        // Hmm I thought this would work automatically
        // This applies the services.yaml of the bundle to the main symfony app.
        return new KrakBoemExtension();
    }
}
