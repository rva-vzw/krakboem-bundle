<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoemBundle\Messenger;

use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\Replay\ReplayBus;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

final readonly class MessengerReplayBus implements ReplayBus
{
    public function __construct(
        private MessageBusInterface $messageBus,
    ) {
    }

    public function publish(Event $event): void
    {
        $this->messageBus->dispatch(new Envelope($event));
    }
}
