<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoemBundle\Messenger;

use RvaVzw\KrakBoem\Cqrs\CommandBus\Command;
use RvaVzw\KrakBoem\Cqrs\CommandBus\CommandBus;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

final readonly class MessengerCommandBus implements CommandBus
{
    public function __construct(
        private MessageBusInterface $messageBus,
    ) {
    }

    public function dispatch(Command ...$commands): void
    {
        foreach ($commands as $command) {
            $this->messageBus->dispatch(
                new Envelope($command)
            );
        }
    }
}
