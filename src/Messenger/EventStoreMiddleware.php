<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoemBundle\Messenger;

use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStore;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;

final readonly class EventStoreMiddleware implements MiddlewareInterface
{
    public function __construct(
        private EventStore $eventStore,
    ) {
    }

    #[\Override]
    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        /** @var Event $event */
        $event = $envelope->getMessage();

        $receivedStamp = $envelope->last(ReceivedStamp::class);
        if (empty($receivedStamp)) {
            // Only record the event if the message wasn't received,
            // see #14
            /** @var AggregateVersionStamp $stamp */
            $stamp = $envelope->last(AggregateVersionStamp::class);
            $this->eventStore->save($event, $stamp->aggregateVersion);
        }

        return $stack->next()->handle($envelope, $stack);
    }
}
