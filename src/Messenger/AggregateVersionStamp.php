<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoemBundle\Messenger;

use Symfony\Component\Messenger\Stamp\StampInterface;

final readonly class AggregateVersionStamp implements StampInterface
{
    public function __construct(
        public int $aggregateVersion,
    ) {
    }
}
