<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoemBundle\Messenger;

use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\EventBus\EventBus;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

final readonly class MessengerEventBus implements EventBus
{
    public function __construct(
        private MessageBusInterface $messageBus,
    ) {
    }

    public function publish(Event $event, int $aggregateVersion): void
    {
        $this->messageBus->dispatch(
            (new Envelope($event))->with(
                new AggregateVersionStamp($aggregateVersion),
            ),
        );
    }
}
