<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoemBundle\Doctrine;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\EventStore\EventStoreHacks;
use RvaVzw\KrakBoem\EventSourcing\EventStore\StoredEvent;
use RvaVzw\KrakBoem\EventSourcing\EventStore\StoredEventRepository;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;
use RvaVzw\KrakBoemBundle\Exception\EventNotFound;
use Webmozart\Assert\Assert;

/**
 * @extends ServiceEntityRepository<OrmStoredEvent>
 */
final class OrmStoredEventRepository extends ServiceEntityRepository implements StoredEventRepository, EventStoreHacks
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrmStoredEvent::class);
    }

    public function save(StoredEvent $storedEvent): void
    {
        $this->_em->persist(OrmStoredEvent::fromStoredEvent($storedEvent));
        $this->_em->flush();
    }

    /**
     * @return \Traversable<StoredEvent>
     */
    public function getAllForAggregate(AggregateRootIdentifier $aggregateRootIdentifier): \Traversable
    {
        $query = $this->createQueryBuilder('e')
            ->andWhere('e.aggregateRootId = :val')
            ->setParameter('val', $aggregateRootIdentifier->toString())
            ->orderBy('e.id', 'ASC')
            ->getQuery();

        foreach ($query->toIterable() as $result) {
            Assert::isInstanceOf($result, OrmStoredEvent::class);
            yield $result->toStoredEvent();

            // Immediately detach result so that it can be garbage collected.
            $this->_em->detach($result);
        }
    }

    public function delete(OrmStoredEvent $storedEvent): void
    {
        $this->_em->remove($storedEvent);
        $this->_em->flush();
    }

    public function hasByAggregateRoot(AggregateRootIdentifier $identifier): bool
    {
        $result = $this->findOneBy(
            ['aggregateRootId' => $identifier->toString()]
        );

        return $result instanceof OrmStoredEvent;
    }

    /**
     * @return \Traversable<StoredEvent>
     */
    public function getStream(): \Traversable
    {
        foreach ($this->findAll() as $storedEvent) {
            yield $storedEvent->toStoredEvent();
            $this->_em->detach($storedEvent);
        }
    }

    public function deleteStreamForAggregate(AggregateRootIdentifier $aggregateRootIdentifier): void
    {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->delete(OrmStoredEvent::class, 'e')
            ->where('e.aggregateRootId = :aggregateRootId')
            ->setParameter('aggregateRootId', $aggregateRootIdentifier->toString());

        $qb->getQuery()->execute();
        $qb->getEntityManager()->flush();
    }

    /**
     * @param array<string,mixed> $payload
     */
    public function updateEventPayload(
        AggregateRootIdentifier $aggregateRootIdentifier,
        int $aggregateVersion,
        array $payload
    ): void {
        $ormStoredEvent = $this->findOneBy([
            'aggregateRootId' => $aggregateRootIdentifier->toString(),
            'aggregateVersion' => $aggregateVersion,
        ]);
        if ($ormStoredEvent instanceof OrmStoredEvent) {
            $ormStoredEvent->setPayload($payload);
            $this->_em->persist($ormStoredEvent);
            $this->_em->flush();

            return;
        }

        throw EventNotFound::byAggregateVersion($aggregateRootIdentifier, $aggregateVersion);
    }

    /**
     * @return \Traversable<StoredEvent>
     */
    public function getAllByEventType(string $eventType): \Traversable
    {
        $query = $this->createQueryBuilder('e')
            ->andWhere('e.eventType = :val')
            ->setParameter('val', $eventType)
            ->orderBy('e.id', 'ASC')
            ->getQuery();

        foreach ($query->toIterable() as $result) {
            Assert::isInstanceOf($result, OrmStoredEvent::class);
            yield $result->toStoredEvent();

            // Immediately detach result so that it can be garbage collected.
            $this->_em->detach($result);
        }
    }

    /**
     * @return \Traversable<StoredEvent>
     */
    public function getByEventTypeAndPartialPayload(string $eventType, string $partialPayload): \Traversable
    {
        $query = $this->createQueryBuilder('e')
            ->andWhere('e.eventType = :type')
            ->andWhere('e.payload like :partialPayload')
            ->setParameter('type', $eventType)
            ->setParameter('partialPayload', "%$partialPayload%")
            ->orderBy('e.id', 'ASC')
            ->getQuery();

        foreach ($query->toIterable() as $result) {
            Assert::isInstanceOf($result, OrmStoredEvent::class);
            yield $result->toStoredEvent();
            $this->_em->detach($result);
        }
    }

    /**
     * @param class-string<Event> $eventType
     */
    public function updateEventType(
        AggregateRootIdentifier $aggregateRootIdentifier,
        int $aggregateVersion,
        string $eventType
    ): void {
        $ormStoredEvent = $this->findOneBy([
            'aggregateRootId' => $aggregateRootIdentifier->toString(),
            'aggregateVersion' => $aggregateVersion,
        ]);
        if ($ormStoredEvent instanceof OrmStoredEvent) {
            $ormStoredEvent->setEventType($eventType);
            $this->_em->persist($ormStoredEvent);
            $this->_em->flush();

            return;
        }

        throw EventNotFound::byAggregateVersion($aggregateRootIdentifier, $aggregateVersion);
    }

    public function getFirstByAggregateEventTypeAndPartialPayload(
        AggregateRootIdentifier $aggregateRootIdentifier,
        string $eventType,
        string $partialPayload,
    ): StoredEvent {
        $query = $this->createQueryBuilder('e')
            ->andWhere('e.aggregateRootId = :aggregateRootId')
            ->andWhere('e.eventType = :type')
            ->andWhere('e.payload like :partialPayload')
            ->setParameter('aggregateRootId', $aggregateRootIdentifier->toString())
            ->setParameter('type', $eventType)
            ->setParameter('partialPayload', "%$partialPayload%")
            ->orderBy('e.id', 'ASC')
            ->getQuery();

        foreach ($query->toIterable() as $result) {
            Assert::isInstanceOf($result, OrmStoredEvent::class);
            $this->_em->detach($result);

            return $result->toStoredEvent();
        }

        throw EventNotFound::byPartialPayload($aggregateRootIdentifier, $eventType, $partialPayload);
    }
}
