<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoemBundle\Doctrine;

use Doctrine\ORM\Mapping as ORM;
use RvaVzw\KrakBoem\EventSourcing\Event;
use RvaVzw\KrakBoem\EventSourcing\EventStore\StoredEvent;
use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;
use RvaVzw\KrakBoem\Id\Uuid4Identifier;

#[ORM\Table(name: 'event_store')]
#[ORM\Entity(repositoryClass: OrmStoredEventRepository::class)]
#[ORM\UniqueConstraint(name: 'aggregate_version', columns: ['aggregate_root_id', 'aggregate_version'])]
class OrmStoredEvent
{
    /**
     * @var ?int
     */
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;
    /**
     * @var non-empty-string
     */
    #[ORM\Column(type: 'guid')]
    private $aggregateRootId;
    /**
     * @var int
     */
    #[ORM\Column(type: 'integer')]
    private $aggregateVersion;
    /**
     * @var string
     */
    #[ORM\Column(type: 'string', length: 255)]
    private $eventType;
    /**
     * @var array<mixed>
     */
    #[ORM\Column(type: 'json')]
    private $payload = [];
    /**
     * @var \DateTimeImmutable
     */
    #[ORM\Column(type: 'datetime_immutable')]
    private $createdAt;

    /**
     * @param non-empty-string $aggregateRootId
     * @param array<mixed> $payload
     */
    private function __construct(
        string $aggregateRootId,
        int $aggregateVersion,
        string $eventType,
        array $payload,
        \DateTimeImmutable $createdAt
    ) {
        $this->aggregateRootId = $aggregateRootId;
        $this->aggregateVersion = $aggregateVersion;
        $this->eventType = $eventType;
        $this->payload = $payload;
        $this->createdAt = $createdAt;
        $this->id = null;
    }

    public static function fromStoredEvent(StoredEvent $event): self
    {
        return new self(
            $event->getAggregateRootIdentifier()->toString(),
            $event->getAggregateVersion(),
            $event->getEventType(),
            $event->getPayload(),
            $event->getCreatedAt()
        );
    }

    public function toStoredEvent(): StoredEvent
    {
        $aggregateRootIdentifier = new readonly class($this->aggregateRootId) extends Uuid4Identifier implements AggregateRootIdentifier {
        };

        return new StoredEvent(
            $aggregateRootIdentifier,
            $this->aggregateVersion,
            $this->eventType,
            $this->payload,
            $this->createdAt
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAggregateRootId(): string
    {
        return $this->aggregateRootId;
    }

    public function getAggregateVersion(): int
    {
        return $this->aggregateVersion;
    }

    public function getEventType(): string
    {
        return $this->eventType;
    }

    /**
     * @return array<mixed>
     */
    public function getPayload(): array
    {
        return $this->payload;
    }

    /**
     * @param array<mixed> $payload
     */
    public function setPayload(array $payload): void
    {
        $this->payload = $payload;
    }

    /**
     * @param class-string<Event> $eventType
     */
    public function setEventType(string $eventType): void
    {
        $this->eventType = $eventType;
    }
}
