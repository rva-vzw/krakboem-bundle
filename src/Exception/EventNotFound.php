<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoemBundle\Exception;

use RvaVzw\KrakBoem\Id\AggregateRootIdentifier;

final class EventNotFound extends \LogicException
{
    public static function byAggregateVersion(AggregateRootIdentifier $aggregateRootIdentifier, int $aggregateVersion): self
    {
        return new self("Event for version {$aggregateVersion} of aggregate {$aggregateRootIdentifier->toString()} not found.");
    }

    public static function byPartialPayload(AggregateRootIdentifier $aggregateRootIdentifier, string $eventType, string $partialPayload): self
    {
        return new self("Event of type {$eventType} for aggregate {$aggregateRootIdentifier->toString()} not found by payload {$partialPayload}.");
    }
}
