<?php

declare(strict_types=1);

namespace RvaVzw\KrakBoemBundle\Command;

use RvaVzw\KrakBoem\EventSourcing\Replay\Replayer;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand('eventsourcing:readmodel:replay')]
final class ReplayCommand extends Command
{
    /** @var Replayer */
    private $replayer;

    public function __construct(Replayer $replayer)
    {
        parent::__construct();
        $this->replayer = $replayer;
    }

    protected function configure(): void
    {
        $this->setDescription('Re-emit all events from the store to the replay bus.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->replayer->replayAll();
        $io->success('Replay completed.');

        return 0;
    }
}
